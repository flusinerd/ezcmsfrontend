import React, { Component } from 'react';
import { AnimatedSwitch } from 'react-router-transition';
import ReactDOM from 'react-dom'
import './App.css';
import PagesList from './pagesList';
import { Router, Route } from 'react-router-dom';
import { createBrowserHistory } from 'history'
import Home from './home';

export const history = createBrowserHistory()

class App extends Component {

  constructor(props) {
    super(props);
    this.pagesLi = React.createRef();
    this.contentLi = React.createRef();
    this.filesLi = React.createRef();
    this.usersLi = React.createRef();
    this.viewLi = React.createRef();
    this.settingsLi = React.createRef();
    this.activeArrow = React.createRef();
  }

  state = {
    arrowStyle: {
      left: 0,
      top: 0,
      height: 30,
      width: 30,
      display: "none",
    },
    activeMode: 0
  }

  setInitialArrow() {
    let newArrowStyle = {};
    Object.assign(newArrowStyle, this.state.arrowStyle);
    let reference = ReactDOM.findDOMNode(this.pagesLi.current).getBoundingClientRect();
    newArrowStyle.height = newArrowStyle.width = reference.height / 3;
    newArrowStyle.left = reference.width - newArrowStyle.width / 2;
    newArrowStyle.top = reference.height * this.state.activeMode + reference.y + reference.height / 2 - newArrowStyle.height / 2;
    this.setState({ arrowStyle: newArrowStyle });
    setTimeout(() => {
      // console.log(window.location.pathname !== "/");
      if (window.location.pathname !== "/") {
        let newArrowStyle = {};
        Object.assign(newArrowStyle, this.state.arrowStyle);
        newArrowStyle.display = "block";
        this.setState({ arrowStyle: newArrowStyle })
      }
    }, 800)
  }

  componentDidMount() {
    setTimeout(() => {
      this.setInitialArrow()
    }, 800);
    window.addEventListener("resize", () => { this.setInitialArrow() });
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.setInitialArrow());
  }


  setArrow(target) {
    switch (target) {
      case 'pages':
        history.push("/pages")
        this.setState({ activeMode: 0 })
        break;
      case 'content':
        history.push("/content")
        this.setState({ activeMode: 1 })
        history.push("/content")
        break;
      case 'files':
        this.setState({ activeMode: 2 })
        history.push("/files")
        break;
      case 'users':
        this.setState({ activeMode: 3 })
        history.push("/users")
        break;
      case 'view':
        this.setState({ activeMode: 4 })
        history.push("/view")
        break;
      case 'settings':
        this.setState({ activeMode: 5 })
        history.push("/settings")
        break;
      default:
        console.log("smth")
        break;
    }
    let element = this[`${target}Li`].current;
    let domElement = ReactDOM.findDOMNode(element);
    let position = domElement.getBoundingClientRect();
    let arrowPosition = {};
    Object.assign(arrowPosition, this.state.arrowStyle)
    arrowPosition.left = position.width - arrowPosition.width / 2;
    arrowPosition.top = position.height / 2 + position.y - arrowPosition.height / 2;
    arrowPosition.display = "block";
    this.setState({ arrowStyle: arrowPosition });
  }

  render() {
    return (
      <Router history={history}>

        <div className="app">
          <div className="navbar">
            <ul className="navbar-list">
              <li className="navbar-warning"><i className="fas fa-exclamation warning"></i><span>5</span></li>
            </ul>
          </div>
          <div className="row">
            <div className="sidebar">
              <div className="arrow" ref={this.activeArrow} style={this.state.arrowStyle}>
              </div>
              <ul className="sidebar-list">
                <li className="sidebar-item pages" onClick={() => { this.setArrow("pages") }} ref={this.pagesLi}><i className="fas fa-file-alt"></i><span>Pages</span></li>
                <li className="sidebar-item content" onClick={() => { this.setArrow("content") }} ref={this.contentLi}><i className="fas fa-box"></i><span>Content</span></li>
                <li className="sidebar-item files" onClick={() => { this.setArrow("files") }} ref={this.filesLi}><i className="fas fa-file-upload"></i><span>Files</span></li>
                <li className="sidebar-item users" onClick={() => { this.setArrow("users") }} ref={this.usersLi}><i className="fas fa-users"></i><span>Users</span></li>
                <li className="sidebar-item view" onClick={() => { this.setArrow("view") }} ref={this.viewLi}><i className="fas fa-scroll"></i><span>View</span></li>
                <li className="sidebar-item settings" onClick={() => { this.setArrow("settings") }} ref={this.settingsLi}><i className="fas fa-cogs"></i><span>Settings</span></li>
              </ul>
            </div>
            <div className="main-content">
              <AnimatedSwitch
                atEnter={{ opacity: 0 }}
                atLeave={{ opacity: 0 }}
                atActive={{ opacity: 1 }}
                className="switch-wrapper"
              >
                <Route path="/pages" activeClassName="fade-in" component={PagesList} />
                <Route component={Home} activeClassName="fade-in" />
              </AnimatedSwitch>
            </div>
          </div>
        </div>

      </Router >
    );
  }
}

export default App;
