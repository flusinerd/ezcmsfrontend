import React, { Component } from 'react';
import './PagesDetail.css';


export default class extends Component {
  constructor(props){
    super(props);
    this.state = {
      pageTitle: "",
      selectedTemplate: this.props.templates[0].name,
    }
  }
  saveChanges() {
    console.log(this.state);
  }

  handleChange(e){
    this.setState({ [e.target.name]: e.target.value })
  }

  render() {
    return (
      <div className="page-detail-container">
        <div className="row center">
          <h2>Page</h2>
        </div>
        <div className="row pageSettings">
          <div className="margin-container">
              <div className="form-group">
                <label>Title</label>
                <input type="text" name="pageTitle" className="w-100" value={this.state.pageTitle} onChange={this.handleChange.bind(this)} />
              </div>
              <div className="form-group">
                <label>Template</label>
                <select className="w-100" name="selectedTemplate" value={this.state.selectedTemplate} onChange={this.handleChange.bind(this)}>
                  {this.props.templates.map(template => <option key={template.id}>{template.name}</option>)}
                </select>
              </div>
              <SubmitButton onClick={()=>{this.saveChanges()}}></SubmitButton>
          </div>
        </div>
      </div>
    );
  }
}

class SubmitButton extends Component {
  render() {
    return (
      <button onClick={this.props.onClick} className="btn btn-success fit-content btn-outline-success mt-1">Save Changes</button>
    );
  }
}