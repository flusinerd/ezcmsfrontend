import React, {Component} from 'react';
import './pagesListButtons.css';

export default class extends Component{
  render(){
    return(
      <div className="pages-list-buttons">
        <button className="btn btn-success"><i className="fas fa-plus-square"></i></button>
        <button className="btn btn-danger"><i className="fas fa-trash"></i></button>
      </div>
    )
  }
}