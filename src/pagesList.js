import React, { Component } from 'react';
import './PagesList.css'
import PagesListButtons from './pagesListButtons'
import PagesDetail from './pagesDetail';

export default class extends Component {
  constructor(props){
    super(props);
    this.pages = [
      {name: 'Page 1', id: 1},
      {name: 'Page 2', id: 2},
      {name: 'Page 3', id: 3},
      {name: 'Page 4', id: 4},
      {name: 'Page 5', id: 5},
    ];
    this.templates=[
      {name: 'Template 1', id: 1},
      {name: 'Template 2', id: 2},
      {name: 'Template 3', id: 3},
      {name: 'Template 4', id: 4},
      {name: 'Template 5', id: 5},
    ]
  }

  render() {
    return (
      <div className="container">
        <div className="pagesList">
          <PagesListButtons />
          <ul>
            {
              this.pages.map(el => <li key={el.id.toString()}><i className="fas fa-file-alt"></i><span>{el.name}</span></li>)
            }
          </ul>
        </div>
            <PagesDetail templates={this.templates}/>
      </div>
    )
  }
}